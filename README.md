# sort-files 

[![npm version](https://badge.fury.io/js/sort-files.svg)](https://badge.fury.io/js/sort-files)

A Node.js script that reads through a directory and sorts files into corresponding folders :open_file_folder:.

## Install

```
$ npm i sort-files
```

## Usage

```js
var sort = require('sort-files');

sort.here("E:\\SORTFILES_SANDBOX\\");
sort.move();
```
